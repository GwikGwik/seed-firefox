//https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
async function postData(url = '', data = "") {
//console.log(url,data);
  const response = await fetch(url, {
    method: 'POST', 
    headers: {
      'Content-Type': 'application/json'
    },
    referrerPolicy: 'no-referrer', 
    body: data
  });
  //return response.json();
  return response.text();
}


async function getData(url = '') {
//console.log(url,data);
  const response = await fetch(url, {
    method: 'GET', 
    headers: {
      'Content-Type': 'application/json'
    },
    referrerPolicy: 'no-referrer'
  });
  //return response.json();
  return response.text();
}


function send_data(){
//console.log("click");
data_get().then((content)=>{
    var url=new URLSearchParams(document.URL);
    var obj={};
if(url.get("select")){
    obj["select"]= url.get("select");
}else{
    obj["select"]= "/";
}
    obj["data"]=content[item_id];
    var string=JSON.stringify(obj);
    console.log(string.length);
    postData('http://nox/seed-editor/data.php', string ).then((msg)=>{console.log(msg);return msg;} ).then(reload_list);
});
}


function reload_list(data){
console.log(data);
    data=JSON.parse(data);
    var list = document.getElementById('data-list');
    list.textContent="";
for (var prop in data) {
  console.log("obj." + prop + " = " + data[prop]);

        const div1 = document.createElement("div");
        div1.textContent=prop;
        //+" "+data[prop];
        div1.onclick= plot_report.bind(data[prop]["read"]);
        list.appendChild(div1);
}

}



function plot_report(){


    var list = document.getElementById('Content');

    if(! list){
    list = document.createElement("div");
    list.innerHTML="";
    list.id="Content";
    list.style="position:fixed;width:500px;height:700px;border-radius:5px;top:150px;right:200px;color:white;background-color:grey;overflow-y:scroll;";
   document.body.appendChild(list);
}
    list.textContent="";
    getData(this).then((msg)=>{console.log(msg);return JSON.parse(msg)["data"];} ).then(render_content);

}



(() => {

  if (window.hasRun) {
    return;
  }
  window.hasRun = true;

    const button = document.createElement("div");
    button.textContent="send";
    //button.classList.add("Send");
    button.onclick=send_data;
    button.style="position:fixed;width:100px;height:100px;border-radius:50px;top:0;right:0;color:white;background-color:grey;";
   document.body.appendChild(button);

    const list = document.createElement("div");
    list.textContent="";
    list.id="data-list";
    list.style="position:fixed;width:200px;height:500px;border-radius:5px;top:150px;right:0;color:white;background-color:grey;";
   document.body.appendChild(list);

    var url=new URLSearchParams(location.search);
console.log(url);
    getData('http://nox/test1.php?action=read&select='+url.get("select")).then((msg)=>{console.log(msg);return msg;} ).then(reload_list);
})();

