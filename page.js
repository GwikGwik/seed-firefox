

//------------------------------------------------------------
function reset(){
//------------------------------------------------------------
console.log(event.target,this);
data_reset().then(()=>{data_get().then(plot_content);});
}
//------------------------------------------------------------
function update(){
//------------------------------------------------------------
//console.log(event.target,this);
data_get().then(()=>{data_get().then(plot_content);});
}
//------------------------------------------------------------
function click_tabs(event){
//------------------------------------------------------------
console.log(event.target,this);
    data_setTab(this,{}).then(update);
}
//------------------------------------------------------------
function plot_tabs(tabs){
//------------------------------------------------------------
    //console.log( tabs);
    for (const tab of tabs) {
        //console.log(tab.title );
        //console.log(tab.url );
        //console.log(tab.audible );
        const div1 = document.createElement("div");
        div1.onclick= click_tabs.bind(tab);
        div1.tab= tab;

        const div = document.createElement("div");
        div.classList.add("Tab");

        const img = document.createElement("img");
        img.src=tab.favIconUrl;
        div.appendChild(img);

        const content = document.createElement("div");
        content.textContent = tab.title || tab.id;
        div.appendChild(content);

        div1.appendChild(div);

        const url = document.createElement("div");
        url.classList.add("Url");
        url.textContent =  tab.url;
        div1.appendChild(url);

        document.getElementById("Tabs").appendChild(div1);
    }
}
//------------------------------------------------------------
browser.tabs.query({  }).then(plot_tabs);


//data_plot();
data_get().then(plot_content);
document.getElementById("reset").onclick=reset;
document.getElementById("send").onclick=send_to_clipboard;
