var item_id="content";
//------------------------------------------------------------
function send_to_clipboard(){
//------------------------------------------------------------
    browser.storage.local.get(item_id).then((content)=>{
    obj=content[item_id];
    string=JSON.stringify(obj);
    console.log(string);
    navigator.clipboard.writeText(string);

});

}
//----------------------------------------------------------
function data_reset(){
//----------------------------------------------------------
    //console.log("reset");
    var data={};
    data[item_id]={i:0};
  return browser.storage.local.set(data);
}
//----------------------------------------------------------
function data_set(data){
//----------------------------------------------------------

     return browser.storage.local.get(item_id).then((content)=>{
//console.log(content);
    obj=content[item_id]
//console.log(obj);
    obj[obj.i]=data;
    obj.i=obj.i+1;
    //console.log(obj);
    var result={};
    result[item_id]=obj;
   browser.storage.local.set(result);
});

}
//----------------------------------------------------------
function data_get(){
//----------------------------------------------------------
    return browser.storage.local.get(item_id);
}

//----------------------------------------------------------
function data_plot(){
//----------------------------------------------------------
     browser.storage.local.get(item_id).then((content)=>{
    //obj=content[item_id]
console.log(content[item_id]);
});
}
//----------------------------------------------------------
function data_setTab(tab,data){
//----------------------------------------------------------

    data["pageicon"]=tab.favIconUrl;
    data["pagecontent"]=tab.title;
    data["pageUrl"]=tab.url;
    return data_set(data);
}
//------------------------------------------------------------
function plot_content(content){
//------------------------------------------------------------
document.getElementById("Content").innerHTML="";
    console.log(content);
    obj=content[item_id];
    render_content(obj);

}
//------------------------------------------------------------
function render_content(obj){
//------------------------------------------------------------
 for (let i = 0; i < obj.i; i++) { 

        const div1 = document.createElement("div");


        const div = document.createElement("div");
        div.classList.add("Content");


if(obj[i].linksrc){
        const link = document.createElement("a");
        link.classList.add("Url");
        link.href=obj[i].linksrc;
        link.innerHTML = "LINK TO "+obj[i].linktext;
        div1.appendChild(link);
}

if(obj[i].content){
        const content = document.createElement("div");
        content.innerHTML = obj[i].content;
        div.appendChild(content);
}
if(obj[i].img){
        const img = document.createElement("img");
        img.style="width:100%";
        img.src = obj[i].img;
        div.appendChild(img);
}
        div1.appendChild(div);


if(obj[i].pagecontent){
        const page = document.createElement("div");
        page.classList.add("Tab");
        page.href=obj[i].pageurl;
if(obj[i].pageicon){
        const img = document.createElement("img");
        img.src=obj[i].pageicon;
        page.appendChild(img);
}
        const content = document.createElement("div");
        content.textContent = obj[i].pagecontent;
        page.appendChild(content);


        div1.appendChild(page);

}
if(obj[i].capture){
        const img1 = document.createElement("img");
        img1.src = obj[i].capture;
        img1.style="width:100%";
        div.appendChild(img1);
}
        const url = document.createElement("a");
        url.href=obj[i].pageUrl;
        url.classList.add("Url");
        url.textContent =  "FROM "+obj[i].pageUrl;
        div1.appendChild(url);

        document.getElementById("Content").appendChild(div1);
    }
}
//------------------------------------------------------------
